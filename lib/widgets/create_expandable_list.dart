import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uranos/widgets/create_list_item.dart';

Widget createList(listOfDocuments, currentUserId, String header) {
  return ExpandableNotifier(
    initialExpanded: true,
    child: Card(
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      child: ScrollOnExpand(
        child: ExpandablePanel(
          header: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              header,
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: Colors.black54,
              ),
            ),
          ),
          expanded: ListView.builder(
            physics: const BouncingScrollPhysics(),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: listOfDocuments.length,
            itemBuilder: (context, index) {
              if (header == 'Invitations') {
                return buildListItemInvitation(
                    context, listOfDocuments[index], currentUserId);
              } else {
                return buildListItemFriend(
                    context, listOfDocuments[index], currentUserId);
              }
            },
          ),
        ),
      ),
    ),
  );
}
