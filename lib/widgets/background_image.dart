import 'package:flutter/cupertino.dart';

class BackgroundImage extends StatelessWidget {
  final Widget child;

  const BackgroundImage({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/arch_angel.jpg'),
        ),
      ),
      child: child,
    );
  }
}
