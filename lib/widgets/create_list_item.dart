import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:uranos/models/User.dart';
import 'package:uranos/screens/chat/chat_screen.dart';
import 'package:uranos/screens/profile_info_screen.dart';
import 'package:uranos/sign_in.dart';
import 'package:uranos/utils/Utils.dart';
import 'package:uranos/widgets/buttons/SmallRaisedButton.dart';

import '../constants.dart';
import 'buttons/PurpleCircleButton.dart';

Widget buildListItemInvitation(
    BuildContext context, DocumentSnapshot document, String currentUserId) {
  return ListTile(
    title: Container(
      decoration: BoxDecoration(
        color: Colors.black38,
        border: Border.all(),
        borderRadius: BorderRadius.circular(8.0),
      ),
      padding: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 10.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: CircleAvatar(
              radius: 28,
              backgroundImage: NetworkImage(document['photoUrlFrom']),
            ),
            flex: 1,
          ),
          Expanded(
            child: Column(
              children: <Widget>[
                Text(document['from']),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SmallRaisedButton(
                      text: 'Accept',
                      onPressedFunction: () async {
                        String senderId = document['fromId'];
                        String senderName = document['from'];
                        String senderPhoto = document['photoUrlFrom'];

                        firestore
                            .collection('users')
                            .document(currentUserId)
                            .collection('friends')
                            .document(senderId)
                            .setData({
                          'friendId': senderId,
                          'displayName': senderName,
                          'photoUrl': senderPhoto,
                        });

                        String userAsJson =
                            await secureStorage.read(key: kCurrentUserKey);
                        User currentUser =
                            User.fromJsonSecureStorage(json.decode(userAsJson));

                        firestore
                            .collection('users')
                            .document(senderId)
                            .collection('friends')
                            .document(currentUserId)
                            .setData({
                          'friendId': currentUserId,
                          'displayName': currentUser.displayName,
                          'photoUrl': currentUser.photoUrl,
                        });

                        firestore
                            .collection('users')
                            .document(currentUserId)
                            .collection('invitation')
                            .document(document.documentID)
                            .delete();

                        String chatId = Utils.getChatId(
                            firstUserId: currentUserId, secondUserId: senderId);

                        firestore
                            .collection('chatRooms')
                            .document(chatId)
                            .setData({});

                        firestore
                            .collection('users')
                            .document(senderId)
                            .collection('chats')
                            .document(chatId)
                            .setData({
                          'chatName': currentUser.displayName,
                          'chatId': chatId,
                        });

                        firestore
                            .collection('users')
                            .document(currentUserId)
                            .collection('chats')
                            .document(chatId)
                            .setData({
                          'chatName': senderName,
                          'chatId': chatId,
                        });
                      },
                      color: Colors.green,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    SmallRaisedButton(
                      text: 'Cancel',
                      onPressedFunction: () {
                        firestore
                            .collection('users')
                            .document(currentUserId)
                            .collection('invitation')
                            .document(document.documentID)
                            .delete();
                      },
                      color: Colors.red,
                    ),
                  ],
                )
              ],
            ),
            flex: 5,
          )
        ],
      ),
    ),
  );
}

Widget buildListItemFriend(
    BuildContext context, DocumentSnapshot document, String currentUserId) {
  return ListTile(
    title: Container(
      decoration: BoxDecoration(
        color: Colors.black38,
        border: Border.all(),
        borderRadius: BorderRadius.circular(8.0),
      ),
      padding: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 10.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: CircleAvatar(
              radius: 28,
              backgroundImage: NetworkImage(document['photoUrl']),
            ),
            flex: 1,
          ),
          Expanded(
            child: Center(
              child: Container(
                child: Text(document['displayName']),
              ),
            ),
            flex: 3,
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 10.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                border: Border.all(),
              ),
              child: IconButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) {
                        String chatId = Utils.getChatId(
                            firstUserId: document['friendId'],
                            secondUserId: currentUserId);
                        return ChatScreen(
                            chatId: chatId,
                            currentUserId: currentUserId,
                            friendId: document['friendId']);
                      },
                    ),
                  );
                },
                icon: Icon(Icons.email),
              ),
            ),
            flex: 1,
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 10.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                border: Border.all(),
              ),
              child: IconButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) {
                      Future<User> friendFuture = Future(() async {
                        DocumentSnapshot snap = await firestore
                            .collection('users')
                            .document(document['friendId'])
                            .get();
                        return User.fromJsonFirebase(
                            json: snap.data, userId: snap.documentID);
                      });

                      return ProfileInfoScreen(
                        document['displayName'],
                        friendFuture,
                        PurpleCircleButton(
                          text: 'Back',
                          onPressedFunction: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      );
                    }),
                  );
                },
                icon: Icon(Icons.account_circle),
              ),
            ),
            flex: 1,
          ),
        ],
      ),
    ),
  );
}
