import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:uranos/models/User.dart';
import 'package:uranos/screens/add_friend/add_friend_screen.dart';
import 'package:uranos/screens/profile_info_screen.dart';
import 'package:uranos/utils/Utils.dart';
import 'package:uranos/widgets/background_image.dart';
import 'package:uranos/widgets/buttons/PurpleCircleButton.dart';
import 'package:uranos/widgets/create_expandable_list.dart';
import 'package:uranos/widgets/LoadingContainer.dart';

import '../constants.dart';
import '../sign_in.dart';
import 'login_page.dart';

class FriendsListScreen extends StatefulWidget {
  @override
  _FriendsListScreen createState() => _FriendsListScreen();
}

class _FriendsListScreen extends State<FriendsListScreen> {
  Future<User> currentUserFuture;

  @override
  void initState() {
    super.initState();
    this.currentUserFuture = Utils.getCurrentUserFutureFromSecureStorage();
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundImage(
      child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: _createAppBar(),
          body: FutureBuilder(
              future: secureStorage.read(key: kCurrentUserKey),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  User currentUser =
                      User.fromJsonSecureStorage(jsonDecode(snapshot.data));
                  return ListView(
                    physics: const BouncingScrollPhysics(),
                    children: <Widget>[
                      StreamBuilder(
                        stream: firestore
                            .collection('users')
                            .document(currentUser.userId)
                            .collection('invitation')
                            .snapshots(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return createList(snapshot.data.documents,
                                currentUser.userId, 'Invitations');
                          } else {
                            return LoadingScreen();
                          }
                        },
                      ),
                      StreamBuilder(
                        stream: firestore
                            .collection('users')
                            .document(currentUser.userId)
                            .collection('friends')
                            .snapshots(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return createList(snapshot.data.documents,
                                currentUser.userId, 'Friends');
                          } else {
                            return LoadingScreen();
                          }
                        },
                      )
                    ],
                  );
                } else {
                  return LoadingScreen();
                }
              }),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => AddFriendScreen(),
              ));
            },
            child: Icon(Icons.add),
          )),
    );
  }

  AppBar _createAppBar() {
    return AppBar(
      title: Text('Friends'),
      leading: Container(),
      actions: <Widget>[
        GestureDetector(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => ProfileInfoScreen(
                  'Your profile',
                  currentUserFuture,
                  PurpleCircleButton(
                    text: 'Sign Out',
                    onPressedFunction: () {
                      signOutGoogle();
                      Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) {
                          return LoginPage();
                        }),
                        ModalRoute.withName('/'),
                      );
                    },
                  ),
                ),
              ),
            );
          },
          child: Container(
            padding: EdgeInsets.fromLTRB(0, 1, 25, 1),
            child: FutureBuilder(
              future: secureStorage.read(key: kCurrentUserKey),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  User currentUser =
                      User.fromJsonSecureStorage(json.decode(snapshot.data));
                  return CircleAvatar(
                    radius: 28,
                    backgroundImage: NetworkImage(currentUser.photoUrl),
                  );
                } else {
                  return CircleAvatar(
                    radius: 28,
                    backgroundImage: AssetImage('assets/uranos.png'),
                  );
                }
              },
            ),
          ),
        ),
      ],
    );
  }
}
