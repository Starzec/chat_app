import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uranos/screens/chat/message_widgets/message_base_class.dart';
import 'package:uranos/screens/full_image_screen.dart';

class MessageImage extends MessageBase {
  MessageImage(
      {@required time,
      @required this.imageUrl,
      @required isMe,
      @required senderDisplayName})
      : super(time: time, isMe: isMe, senderDisplayName: senderDisplayName);

  final String imageUrl;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(isMe ? 120 : 0, 0, isMe ? 0 : 120, 0),
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment:
              isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              senderDisplayName,
              style: TextStyle(
                fontSize: 10.0,
                color: Colors.white,
              ),
            ),
            Container(
              child: FlatButton(
                child: Material(
                  child: CachedNetworkImage(
                    placeholder: (context, url) => Container(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                      ),
                      padding: EdgeInsets.all(70.0),
                      decoration: BoxDecoration(
                        color: Colors.white70,
                        borderRadius: BorderRadius.all(
                          Radius.circular(8.0),
                        ),
                      ),
                    ),
                    errorWidget: (context, url, error) => Material(
                      child: Image.asset(
                        'assets/no_image.jpg',
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(8.0),
                      ),
                      clipBehavior: Clip.hardEdge,
                    ),
                    imageUrl: imageUrl,
                    fit: BoxFit.cover,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  clipBehavior: Clip.hardEdge,
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => FullImageScreen(imageUrl: imageUrl),
                    ),
                  );
                },
                padding: EdgeInsets.all(0),
              ),
            )
          ],
        ),
      ),
    );
  }
}
