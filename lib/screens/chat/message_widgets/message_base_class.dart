import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

abstract class MessageBase extends StatelessWidget {
  final Timestamp time;
  final bool isMe;
  final String senderDisplayName;

  MessageBase(
      {@required this.time,
      @required this.isMe,
      @required this.senderDisplayName});
}
