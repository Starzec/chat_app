import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:uranos/screens/chat/chat_screen.dart';
import 'package:uranos/screens/chat/message_widgets/message_base_class.dart';
import 'package:uranos/screens/chat/message_widgets/message_image.dart';

import '../../constants.dart';
import 'message_widgets/message_bubble.dart';

class MessagesStream extends StatelessWidget {
  final String chatId;
  final String currentUserId;

  MessagesStream({Key key, @required this.chatId, @required this.currentUserId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: firestore
          .collection('chatRooms')
          .document(chatId)
          .collection('messages')
          .snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.lightBlueAccent,
            ),
          );
        }
        final messages = snapshot.data.documents.reversed;
        List<MessageBase> messageBubbles = [];
        for (var message in messages) {
          final messageText = message.data['text'];
          final messageSenderId = message.data['senderId'];
          final messageSenderDisplayName = message.data['senderDisplayName'];
          final timestamp = message.data['time'];

          if (message.data['type'] == ChatScreen.kMessageType) {
            final messageBubble = MessageBubble(
              senderDisplayName: messageSenderDisplayName,
              text: messageText,
              isMe: currentUserId == messageSenderId,
              time: timestamp,
            );
            messageBubbles.add(messageBubble);
            messageBubbles.sort((a, b) => b.time.compareTo(a.time));
          } else {
            final messageImage = MessageImage(
              senderDisplayName: messageSenderDisplayName,
              isMe: currentUserId == messageSenderId,
              time: timestamp,
              imageUrl: messageText,
            );
            messageBubbles.add(messageImage);
            messageBubbles.sort((a, b) => b.time.compareTo(a.time));
          }
        } // for message
        return Expanded(
          child: ListView(
            addAutomaticKeepAlives: false,
            reverse: true,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
            children: messageBubbles,
          ),
        );
      },
    );
  }
}
