import 'dart:convert';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uranos/constants.dart';
import 'package:uranos/models/Invitation.dart';
import 'package:uranos/models/User.dart';
import 'package:uranos/widgets/background_image.dart';
import 'package:uranos/widgets/buttons/PurpleCircleButton.dart';
import 'package:uranos/widgets/UserInfoContainer.dart';
import 'package:uranos/screens/add_friend/UserNotFound.dart';
import '../../sign_in.dart';

class AddFriendScreen extends StatefulWidget {
  @override
  _AddFriendScreen createState() => _AddFriendScreen();
}

class _AddFriendScreen extends State<AddFriendScreen> {
  String _emailInput = '';
  User _currentSearchedUser;
  User _currentUser;

  @override
  void initState() {
    super.initState();
    _readCurrentUser();
  }

  _readCurrentUser() async {
    String jsonUser = await secureStorage.read(key: kCurrentUserKey);
    setState(() {
      _currentUser = User.fromJsonSecureStorage(jsonDecode(jsonUser));
    });
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundImage(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: Text('Add new friend'),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Padding(
                child: TextField(
                  keyboardType: TextInputType.emailAddress,
                  autofocus: true,
                  decoration: kMessageTextFieldDecoration(
                      hintText: 'Enter friends email address'),
                  onChanged: (updatedText) {
                    _emailInput = updatedText;
                  },
                ),
                padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
              ),
              SizedBox(
                height: 15,
              ),
              FlatButton(
                onPressed: () {
                  _showBottomSheet(context);
                },
                child: Container(
                  height: 50,
                  width: 150,
                  decoration: kNormalButtonBoxDecoration,
                  child: Center(
                    child: Text(
                      'Search',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _showBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            decoration: kInfoScreenBoxDecoration,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FutureBuilder(
                    future: firestore
                        .collection('users')
                        .where('email', isEqualTo: _emailInput.trim())
                        .getDocuments(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return _loadingText();
                      }
                      List<DocumentSnapshot> documents =
                          snapshot.data.documents;
                      if (documents.length == 0) {
                        return UserNotFound();
                      } else {
                        dynamic document = documents[0];
                        _currentSearchedUser = User(
                            document.documentID,
                            document['displayName'],
                            document['email'],
                            document['photoUrl']);
                        return UserInfoColumn(
                          user: _currentSearchedUser,
                          button: PurpleCircleButton(
                            text: 'Invite',
                            onPressedFunction: () {
                              Navigator.of(context).pop();
                              _inviteUser();
                            },
                          ),
                        );
                      }
                    }),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          );
        });
  }

  void _inviteUser() {
    firestore
        .collection('users')
        .document(_currentSearchedUser.userId)
        .collection('invitation')
        .document()
        .setData(Invitation(_currentUser, _currentSearchedUser).toJson());
  }

  Widget _loadingText() {
    return TypewriterAnimatedTextKit(
      totalRepeatCount: 20,
      speed: Duration(milliseconds: 700),
      text: [
        '...',
      ],
      textStyle: TextStyle(
        fontSize: 45.0,
        fontWeight: FontWeight.w900,
      ),
    );
  }
}
