import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

final Firestore firestore = Firestore.instance;
const String kCurrentUserKey = 'currentUser';

var kNormalButtonBoxDecoration = BoxDecoration(
  color: Colors.blue,
  borderRadius: BorderRadius.circular(30),
  boxShadow: kElevationToShadow[4],
);

var kInfoScreenBoxDecoration = BoxDecoration(
  gradient: LinearGradient(
    begin: Alignment.topRight,
    end: Alignment.bottomLeft,
//    colors: [Colors.white, Colors.black],
    colors: [Colors.cyanAccent[100], Colors.blue[900]],
  ),
);

const kSendButtonTextStyle = TextStyle(
  color: Colors.lightBlueAccent,
  fontWeight: FontWeight.bold,
  fontSize: 18.0,
);

InputDecoration kMessageTextFieldDecoration({hintText: String}) {
  return InputDecoration(
    contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
    hintText: hintText,
    border: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
    fillColor: Colors.black12,
    filled: true,
  );
}

const kMessageContainerDecoration = BoxDecoration(
  border: Border(
    top: BorderSide(color: Colors.lightBlueAccent, width: 2.0),
  ),
);
